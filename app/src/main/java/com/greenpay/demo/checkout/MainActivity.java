package com.greenpay.demo.checkout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.greenpay.demo.checkout.process.GreenPayProcess;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void checkoutCard(android.view.View view){
        GreenPayProcess.checkoutCardGreenPayProcess(this);
    }

    public void tokenizeCard(android.view.View view){
        GreenPayProcess.tokenizeCardGreenPayProcess(this);
    }
}
