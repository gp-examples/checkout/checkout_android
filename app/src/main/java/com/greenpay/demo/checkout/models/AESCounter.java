package com.greenpay.demo.checkout.models;

public class AESCounter {

    private int counter;

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public AESCounter(int counter) {
        this.counter = counter;
    }

    /**
     Nombre: getCounterValue
     Descripción: Obtiene el valor del Counter como un arreglo de bytes.
     Retorna: el valor del Counter como un arreglo de bytes.
     */
    public byte[] getCounterValue(){
        byte[] counterBytes = new byte[16];
        byte[] intBytes = toByteArray(counter);
        int counterReverseIndex = 15;
        for(int i=intBytes.length-1; i >= 0; --i){
            counterBytes[counterReverseIndex] = intBytes[i];
            --counterReverseIndex;
        }
        return counterBytes;
    }

    /**
     Nombre: toByteArray
     Descripción: Convierte el valor entero a un arreglo de bytes.
     Retorna: el valor del Counter como un arreglo de bytes.
     */
    public byte[] toByteArray(int value) {
        return new byte[] {
                (byte)(value >> 24),
                (byte)(value >> 16),
                (byte)(value >> 8),
                (byte)value};
    }
}
