package com.greenpay.demo.checkout.models;

public class AESKey {

    private int[] key;

    public AESKey(int[] key) {
        this.key = key;
    }

    public int[] getKey() {
        return key;
    }

    public void setKey(int[] key) {
        this.key = key;
    }

    /**
     Nombre: getKeyValue
     Descripción: Obtiene el valor de la llave como un arreglo de bytes.
     Retorna: el valor de la llave como un arreglo de bytes.
     */
    public byte[] getKeyValue(){
        byte[] keyBytes = new byte[16];
        for(int i=0; i < 16; ++i){
            Integer integer = Integer.valueOf(key[i]);
            keyBytes[i] = integer.byteValue();
        }

        return keyBytes;
    }
}
