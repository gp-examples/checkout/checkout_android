package com.greenpay.demo.checkout.models;

import com.google.gson.annotations.SerializedName;

public class AESPair {

    @SerializedName("k")
    private int[] key;
    @SerializedName("s")
    private int couner;

    public AESPair() {
    }

    public AESPair(int[] key, int couner) {
        this.key = key;
        this.couner = couner;
    }

    public int[] getKey() {
        return key;
    }

    public void setKey(int[] key) {
        this.key = key;
    }

    public int getCouner() {
        return couner;
    }

    public void setCouner(int couner) {
        this.couner = couner;
    }
}
