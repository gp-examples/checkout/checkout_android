package com.greenpay.demo.checkout.models;

import com.google.gson.annotations.SerializedName;

public class BaseOrderRequestData {

    @SerializedName("secret")
    private String secret;

    @SerializedName("merchantId")
    private String merchantId;

    public BaseOrderRequestData() {
        this.secret = "";
        this.merchantId = "";
    }

    public BaseOrderRequestData(String secret, String merchantId) {
        this.secret = secret;
        this.merchantId = merchantId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
}
