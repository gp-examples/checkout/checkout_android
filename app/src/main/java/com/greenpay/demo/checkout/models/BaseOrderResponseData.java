package com.greenpay.demo.checkout.models;

import com.google.gson.annotations.SerializedName;

public class BaseOrderResponseData {

    @SerializedName("session")
    private String session;

    @SerializedName("token")
    private String token;

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public BaseOrderResponseData(String session, String token) {
        this.session = session;
        this.token = token;
    }

    public BaseOrderResponseData(){
        this.session = "";
        this.token = "";
    }
}
