package com.greenpay.demo.checkout.models;

import com.google.gson.annotations.SerializedName;

public class CardData extends CreditCard{

    @SerializedName("card")
    private CardInfo cardInfo;

    public CardInfo getCardInfo() {
        return cardInfo;
    }

    public void setCardInfo(CardInfo cardInfo) {
        this.cardInfo = cardInfo;
    }

    public CardData() {
    }

    public CardData(CardInfo cardInfo) {
        this.cardInfo = cardInfo;
    }
}
