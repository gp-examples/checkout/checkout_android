package com.greenpay.demo.checkout.models;

import com.google.gson.annotations.SerializedName;

public class CardExpirationDate {

    @SerializedName("month")
    private int month;

    @SerializedName("year")
    private int year;

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public CardExpirationDate(int month, int year) {
        this.month = month;
        this.year = year;
    }
}
