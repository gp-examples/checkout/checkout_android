package com.greenpay.demo.checkout.models;

import com.google.gson.annotations.SerializedName;

public class CardInfo {

    @SerializedName("cardHolder")
    private String cardHolder;

    @SerializedName("expirationDate")
    private CardExpirationDate cardExpirationDate;

    @SerializedName("cardNumber")
    private String cardNumber;

    @SerializedName("cvc")
    private String cvc;

    @SerializedName("nickname")
    private String nickname;

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public CardExpirationDate getCardExpirationDate() {
        return cardExpirationDate;
    }

    public void setCardExpirationDate(CardExpirationDate cardExpirationDate) {
        this.cardExpirationDate = cardExpirationDate;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public CardInfo() {
    }

    public CardInfo(String cardHolder, CardExpirationDate cardExpirationDate, String cardNumber, String cvc, String nickname) {
        this.cardHolder = cardHolder;
        this.cardExpirationDate = cardExpirationDate;
        this.cardNumber = cardNumber;
        this.cvc = cvc;
        this.nickname = nickname;
    }
}
