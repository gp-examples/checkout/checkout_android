package com.greenpay.demo.checkout.models;

import com.google.gson.annotations.SerializedName;

public class CheckoutRequestData {

    @SerializedName("session")
    private String session;

    @SerializedName("ld")
    private String ld;

    @SerializedName("lk")
    private String lk;

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getLd() {
        return ld;
    }

    public void setLd(String ld) {
        this.ld = ld;
    }

    public String getLk() {
        return lk;
    }

    public void setLk(String lk) {
        this.lk = lk;
    }

    public CheckoutRequestData() {
    }

    public CheckoutRequestData(String session, String ld, String lk) {
        this.session = session;
        this.ld = ld;
        this.lk = lk;
    }
}
