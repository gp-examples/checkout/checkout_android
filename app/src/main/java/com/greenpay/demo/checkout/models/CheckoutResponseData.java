package com.greenpay.demo.checkout.models;

import com.google.gson.annotations.SerializedName;

public class CheckoutResponseData {

    @SerializedName("status")
    private int status;

    @SerializedName("orderId")
    private String orderId;

    @SerializedName("authorization")
    private String authorization;

    @SerializedName("last4")
    private String last4;

    @SerializedName("brand")
    private String brand;

    @SerializedName("result")
    private CheckoutResponseDataResult result;

    @SerializedName("_signature")
    private String signature;

    public CheckoutResponseData(int status, String orderId, String authorization, String last4, String brand, CheckoutResponseDataResult result, String signature) {
        this.status = status;
        this.orderId = orderId;
        this.authorization = authorization;
        this.last4 = last4;
        this.brand = brand;
        this.result = result;
        this.signature = signature;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getLast4() {
        return last4;
    }

    public void setLast4(String last4) {
        this.last4 = last4;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public CheckoutResponseDataResult getResult() {
        return result;
    }

    public void setResult(CheckoutResponseDataResult result) {
        this.result = result;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
