package com.greenpay.demo.checkout.models;

import com.google.gson.annotations.SerializedName;

public class CheckoutResponseDataResult {

    @SerializedName("time_local_tran")
    private String timeLocalTran;

    @SerializedName("systems_trace_audit_number")
    private String systemsTraceAuditNumber;

    @SerializedName("success")
    private boolean success;

    @SerializedName("retrieval_ref_num")
    private String retrievalRefNum;

    @SerializedName("resp_code")
    private String respCode;

    @SerializedName("reserved_private4")
    private String reservedPrivate4;

    @SerializedName("proc_code")
    private String procCode;

    @SerializedName("network_international_id")
    private String networkInternationalId;

    @SerializedName("mti")
    private String mti;

    @SerializedName("merchandt_id")
    private String merchandId;

    @SerializedName("date_local_tran")
    private String dateLocalTran;

    @SerializedName("card_acceptor_terminal_id")
    private String cardAcceptorTerminalId;

    @SerializedName("authorization_id_resp")
    private String authorizationIdResp;

    public String getTimeLocalTran() {
        return timeLocalTran;
    }

    public void setTimeLocalTran(String timeLocalTran) {
        this.timeLocalTran = timeLocalTran;
    }

    public String getSystemsTraceAuditNumber() {
        return systemsTraceAuditNumber;
    }

    public void setSystemsTraceAuditNumber(String systemsTraceAuditNumber) {
        this.systemsTraceAuditNumber = systemsTraceAuditNumber;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getRetrievalRefNum() {
        return retrievalRefNum;
    }

    public void setRetrievalRefNum(String retrievalRefNum) {
        this.retrievalRefNum = retrievalRefNum;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getReservedPrivate4() {
        return reservedPrivate4;
    }

    public void setReservedPrivate4(String reservedPrivate4) {
        this.reservedPrivate4 = reservedPrivate4;
    }

    public String getProcCode() {
        return procCode;
    }

    public void setProcCode(String procCode) {
        this.procCode = procCode;
    }

    public String getNetworkInternationalId() {
        return networkInternationalId;
    }

    public void setNetworkInternationalId(String networkInternationalId) {
        this.networkInternationalId = networkInternationalId;
    }

    public String getMti() {
        return mti;
    }

    public void setMti(String mti) {
        this.mti = mti;
    }

    public String getMerchandId() {
        return merchandId;
    }

    public void setMerchandId(String merchandId) {
        this.merchandId = merchandId;
    }

    public String getDateLocalTran() {
        return dateLocalTran;
    }

    public void setDateLocalTran(String dateLocalTran) {
        this.dateLocalTran = dateLocalTran;
    }

    public String getCardAcceptorTerminalId() {
        return cardAcceptorTerminalId;
    }

    public void setCardAcceptorTerminalId(String cardAcceptorTerminalId) {
        this.cardAcceptorTerminalId = cardAcceptorTerminalId;
    }

    public String getAuthorizationIdResp() {
        return authorizationIdResp;
    }

    public void setAuthorizationIdResp(String authorizationIdResp) {
        this.authorizationIdResp = authorizationIdResp;
    }

    public CheckoutResponseDataResult(String timeLocalTran, String systemsTraceAuditNumber, boolean success, String retrievalRefNum, String respCode, String reservedPrivate4, String procCode, String networkInternationalId, String mti, String merchandId, String dateLocalTran, String cardAcceptorTerminalId, String authorizationIdResp) {
        this.timeLocalTran = timeLocalTran;
        this.systemsTraceAuditNumber = systemsTraceAuditNumber;
        this.success = success;
        this.retrievalRefNum = retrievalRefNum;
        this.respCode = respCode;
        this.reservedPrivate4 = reservedPrivate4;
        this.procCode = procCode;
        this.networkInternationalId = networkInternationalId;
        this.mti = mti;
        this.merchandId = merchandId;
        this.dateLocalTran = dateLocalTran;
        this.cardAcceptorTerminalId = cardAcceptorTerminalId;
        this.authorizationIdResp = authorizationIdResp;
    }
}
