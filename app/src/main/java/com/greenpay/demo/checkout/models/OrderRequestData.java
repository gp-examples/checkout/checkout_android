package com.greenpay.demo.checkout.models;

import com.google.gson.annotations.SerializedName;

public class OrderRequestData extends BaseOrderRequestData {

    @SerializedName("terminal")
    private String terminal;

    @SerializedName("amount")
    private int amount;

    @SerializedName("currency")
    private String currency;

    @SerializedName("description")
    private String description;

    @SerializedName("orderReference")
    private String orderReference;

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrderReference() {
        return orderReference;
    }

    public void setOrderReference(String orderReference) {
        this.orderReference = orderReference;
    }

    public OrderRequestData() {
        this.terminal = "";
        this.amount = 0;
        this.currency = "";
        this.description = "";
        this.orderReference = "";
    }

    public OrderRequestData(String terminal, int amount, String currency, String description, String orderReference) {
        this.terminal = terminal;
        this.amount = amount;
        this.currency = currency;
        this.description = description;
        this.orderReference = orderReference;
    }
}
