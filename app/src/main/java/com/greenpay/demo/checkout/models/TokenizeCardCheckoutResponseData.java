package com.greenpay.demo.checkout.models;

import com.google.gson.annotations.SerializedName;

public class TokenizeCardCheckoutResponseData {

    @SerializedName("status")
    private int status;

    @SerializedName("expiration_date")
    private String expirationDate;

    @SerializedName("brand")
    private String brand;

    @SerializedName("nickname")
    private String nickname;

    @SerializedName("callback")
    private String callback;

    @SerializedName("result")
    private TokenizeCardCheckoutResponseDataResult result;

    @SerializedName("_signature")
    private String signature;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    public TokenizeCardCheckoutResponseDataResult getResult() {
        return result;
    }

    public void setResult(TokenizeCardCheckoutResponseDataResult result) {
        this.result = result;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public TokenizeCardCheckoutResponseData(int status, String expirationDate, String brand, String nickname, String callback, TokenizeCardCheckoutResponseDataResult result, String signature) {
        this.status = status;
        this.expirationDate = expirationDate;
        this.brand = brand;
        this.nickname = nickname;
        this.callback = callback;
        this.result = result;
        this.signature = signature;
    }
}
