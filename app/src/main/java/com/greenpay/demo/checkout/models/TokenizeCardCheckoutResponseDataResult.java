package com.greenpay.demo.checkout.models;

import com.google.gson.annotations.SerializedName;

public class TokenizeCardCheckoutResponseDataResult {

    @SerializedName("token")
    private String token;

    @SerializedName("last_digits")
    private String lastDigits;

    @SerializedName("bin")
    private String bin;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLastDigits() {
        return lastDigits;
    }

    public void setLastDigits(String lastDigits) {
        this.lastDigits = lastDigits;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public TokenizeCardCheckoutResponseDataResult(String token, String lastDigits, String bin) {
        this.token = token;
        this.lastDigits= lastDigits;
        this.bin = bin;
    }
}
