package com.greenpay.demo.checkout.models;

import com.google.gson.annotations.SerializedName;

public class TokenizeCardOrderRequestData extends BaseOrderRequestData{

    @SerializedName("requestId")
    private String requestId;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }


    public TokenizeCardOrderRequestData() {
        this.requestId = "";
    }

    public TokenizeCardOrderRequestData(String requestId) {
        this.requestId = requestId;
    }
}
