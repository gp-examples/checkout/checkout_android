package com.greenpay.demo.checkout.models;

import com.google.gson.annotations.SerializedName;

public class TokenizedCard extends CreditCard{

    @SerializedName("token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public TokenizedCard() {
    }

    public TokenizedCard(String token) {
        this.token = token;
    }
}
