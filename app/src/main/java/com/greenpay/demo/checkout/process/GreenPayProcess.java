package com.greenpay.demo.checkout.process;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.greenpay.demo.checkout.models.AESCounter;
import com.greenpay.demo.checkout.models.AESKey;
import com.greenpay.demo.checkout.models.AESPair;
import com.greenpay.demo.checkout.models.BaseOrderResponseData;
import com.greenpay.demo.checkout.models.CardData;
import com.greenpay.demo.checkout.models.CardExpirationDate;
import com.greenpay.demo.checkout.models.CardInfo;
import com.greenpay.demo.checkout.models.CheckoutRequestData;
import com.greenpay.demo.checkout.models.CheckoutResponseData;
import com.greenpay.demo.checkout.models.CreditCard;
import com.greenpay.demo.checkout.models.OrderRequestData;
import com.greenpay.demo.checkout.models.OrderResponseData;
import com.greenpay.demo.checkout.models.TokenizeCardCheckoutResponseData;
import com.greenpay.demo.checkout.models.TokenizeCardOrderRequestData;
import com.greenpay.demo.checkout.models.TokenizeCardOrderResponseData;
import com.greenpay.demo.checkout.models.TokenizedCard;
import com.greenpay.demo.checkout.utils.AESUtils;
import com.greenpay.demo.checkout.utils.CipherUtils;
import com.greenpay.demo.checkout.utils.GsonRequest;
import com.greenpay.demo.checkout.utils.ResponseCallback;
import com.greenpay.demo.checkout.utils.StringUtils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class GreenPayProcess {

    static String GreenPaySecret = "";
    static String GreenPayMerchandId = "";
    static String GreenPayTerminal = "";

    // Replace with your own Publick Key in this format
    static String GreenPayPublicKey = "-----BEGIN PUBLIC KEY-----" +
            "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCQGA+x/Z7WMLuQU2gawuJ4u9lP" +
            "0XBBphHKbmYxnqzfnXGuRWfLred588td37zv95p2dh6fcuII5w3G+KXBNV2DVhwa" +
            "N3qG9ihoFKaHtFlQApFsLK1DO1G0KZ6rhBn6Bny5aWCS8I1Hq0KuVzHsgsf2bvxc" +
            "/IBIfLn7lWgVcGzh+QIDAQAB" +
            "-----END PUBLIC KEY-----";

    static CardData cardData;

    /**
     Nombre: checkoutCardGreenPayProcess
     Descripción: Comienza el proceso de compra contra la plataforma Greenpay.
     Parámetros: context - Contexto de Android para realizar los llamados al API DE GreenPay
     */
    public static void checkoutCardGreenPayProcess(final Context context){
        Log.d("GreenPay", "Starting GreenPay Checkout Card process: ");

        CardInfo cardInfo = new CardInfo();
        cardInfo.setCardHolder("Jhon Doe");
        cardInfo.setCardExpirationDate(new CardExpirationDate(9,21));
        cardInfo.setCardNumber("4795736054664396");
        cardInfo.setCvc("123");
        cardInfo.setNickname("visa2449");

        cardData = new CardData();
        cardData.setCardInfo(cardInfo);

        final Gson gson = new GsonBuilder().disableHtmlEscaping().create();

        // El orden para ejecutar la transacción:
        // 1. Crear una nueva orden.
        // 2. Realizar el proceso de Checkout de la orden.
        // 3. Verificar la firma de la respuesta de Greenpay para garantizar la integridad del mensaje.

        OrderRequestData orderRequestData = new OrderRequestData();
        orderRequestData.setSecret(GreenPayProcess.GreenPaySecret);
        orderRequestData.setMerchantId(GreenPayProcess.GreenPayMerchandId);
        orderRequestData.setTerminal(GreenPayProcess.GreenPayTerminal);
        orderRequestData.setAmount(1);
        orderRequestData.setCurrency("");
        orderRequestData.setDescription("desc");
        orderRequestData.setOrderReference("1");

        GreenPayProcess.postCreateOrder(orderRequestData, context, new ResponseCallback<OrderResponseData>(){

            public void onSuccess(OrderResponseData response){

                Log.d("GreenPay", "Create Order Response From Parent: " + gson.toJson(response));


                Log.d("GreenPay", "Before Checkout");
                GreenPayProcess.postCheckout(cardData, response, context, new ResponseCallback<CheckoutResponseData>() {
                    @Override
                    public void onSuccess(CheckoutResponseData response) {
                        Log.d("GreenPay", "Create Checkout Response From Parent: " + gson.toJson(response));

                        GreenPayProcess.verifyCheckoutResponse(response);
                    }
                });
            }
        });
    }

    /**
     Nombre: startGreenPayProcess
     Descripción: Comienza el proceso de tokenizar una tarjeta contra la plataforma Greenpay.
     Parámetros: context - Contexto de Android para realizar los llamados al API DE GreenPay
     */
    public static void tokenizeCardGreenPayProcess(final Context context){
        Log.d("GreenPay", "Starting GreenPay Tokenize Card process: ");

        CardInfo cardInfo = new CardInfo();
        cardInfo.setCardHolder("Jhon Doe");
        cardInfo.setCardExpirationDate(new CardExpirationDate(9,21));
        cardInfo.setCardNumber("4795736054664396");
        cardInfo.setCvc("123");
        cardInfo.setNickname("visa2449");

        cardData = new CardData();
        cardData.setCardInfo(cardInfo);

        final Gson gson = new GsonBuilder().disableHtmlEscaping().create();

        // El orden para realizar la tokenización de una tarjeta:
        // 1. Crear una orden para tokenizar la tarjeta.
        // 2. Realizar el proceso de Checkout para la orden de tokenización.
        // 3. Verificar la firma de la respuesta de GreenPay para garantizar la integridad del mensaje.
        final TokenizeCardOrderRequestData tokenizeCardOrderRequestData = new TokenizeCardOrderRequestData();
        tokenizeCardOrderRequestData.setSecret(GreenPayProcess.GreenPaySecret);
        tokenizeCardOrderRequestData.setMerchantId(GreenPayProcess.GreenPayMerchandId);
        tokenizeCardOrderRequestData.setRequestId("1");

        GreenPayProcess.postTokenizeCardCreateOrder(tokenizeCardOrderRequestData, context, new ResponseCallback<TokenizeCardOrderResponseData>(){

            public void onSuccess(TokenizeCardOrderResponseData response){

                Log.d("GreenPay", "Tokenize Card Create Order Response From Parent: " + gson.toJson(response));


                Log.d("GreenPay", "Before Tokenize Card Checkout");
                GreenPayProcess.postTokenizeCardCheckout(cardData, response, context, new ResponseCallback<TokenizeCardCheckoutResponseData>() {
                    @Override
                    public void onSuccess(TokenizeCardCheckoutResponseData response) {
                        Log.d("GreenPay", "Tokenize Card Create Checkout Response From Parent: " + gson.toJson(response));

                        GreenPayProcess.verifyTokenizeCardCheckoutResponse(response, tokenizeCardOrderRequestData.getRequestId());

                        // El orden para realizar la el pago de una orden con una tarjta tokenizada:
                        // 1. Obtener el token de la tarjeta previamente tokenizada.
                        // 2. Crear una orden para tokenizar la tarjeta.
                        // 3. Realizar el proceso de Checkout para la orden de tokenización.
                        // 4. Verificar la firma de la respuesta de GreenPay para garantizar la integridad del mensaje.

                        final TokenizedCard tokenizedCard = new TokenizedCard();
                        tokenizedCard.setToken(response.getResult().getToken());

                        OrderRequestData orderRequestData = new OrderRequestData();
                        orderRequestData.setSecret(GreenPayProcess.GreenPaySecret);
                        orderRequestData.setMerchantId(GreenPayProcess.GreenPayMerchandId);
                        orderRequestData.setTerminal(GreenPayProcess.GreenPayTerminal);
                        orderRequestData.setAmount(1);
                        orderRequestData.setCurrency("");
                        orderRequestData.setDescription("desc");
                        orderRequestData.setOrderReference("1");

                        GreenPayProcess.postCreateOrder(orderRequestData, context, new ResponseCallback<OrderResponseData>(){

                            public void onSuccess(OrderResponseData response){

                                Log.d("GreenPay", "Create Order Response From Parent: " + gson.toJson(response));


                                Log.d("GreenPay", "Before Checkout");
                                GreenPayProcess.postCheckout(tokenizedCard, response, context, new ResponseCallback<CheckoutResponseData>() {
                                    @Override
                                    public void onSuccess(CheckoutResponseData response) {
                                        Log.d("GreenPay", "Create Checkout Response From Parent: " + gson.toJson(response));

                                        GreenPayProcess.verifyCheckoutResponse(response);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    /**
     Nombre: postCreateOrder
     Descripción: Realiza el proceso de creación de una nueva orden en la plataforma Greenpay.
     Retorna: El objeto OrderResponseData que contiene la respuesta a la creación de la orden.
     Parámetros: - OrderResponseData: Objeto OrderRequestData con los datos para crear una nueva orden.
                 - context - Contexto de Android para realizar los llamados al API DE GreenPay
     */
    public static void postCreateOrder(OrderRequestData orderRequestData, Context context, final ResponseCallback<OrderResponseData> responseCallback){
        Log.d("GreenPay", "Creating Order:");
        String orderURL = "https://sandbox-merchant.greenpay.me";

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-type", "application/json");

        GsonRequest<OrderResponseData> postOrderRequest = new GsonRequest<OrderResponseData>(Request.Method.POST, orderURL, orderRequestData, OrderResponseData.class, headers, new Response.Listener<OrderResponseData>() {
            @Override
            public void onResponse(OrderResponseData response) {
                Gson gson = new GsonBuilder().disableHtmlEscaping().create();
                Log.d("GreenPay", "Create Order Response: " + gson.toJson(response));

                responseCallback.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("GreenPay", "GreenPay error");

                String body;
                String statusCode = String.valueOf(error.networkResponse.statusCode);
                Log.e("GreenPay", "GreenPay Error HTTP Error: " + statusCode);
                if(error.networkResponse.data!=null) {
                    try {
                        body = new String(error.networkResponse.data,"UTF-8");
                        Log.e("GreenPay", "GreenPay Error Body: " + body);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(postOrderRequest);
    }

    /**
     Nombre: PostCheckout
     Descripción: Realiza el proceso de checkout en la plataforma Greenpay.
     Retorna: El objeto CheckoutResponseData que contiene la respuesta del proceso de Checkout.
     Parámetros: - cardData: Objeto que representa la tarjeta del cliente.
                 - orderResponseData: Objeto orderResponseData con los datos para hacer checkout.
                 - context - Contexto de Android para realizar los llamados al API DE GreenPay
     */
    public static void postCheckout(CreditCard cardData, OrderResponseData orderResponseData, Context context, final ResponseCallback<CheckoutResponseData> responseCallback){
        Log.d("GreenPay", "Checking out Order:");
        String checkoutURL = "https://sandbox-checkout.greenpay.me";

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-type", "application/json");
        headers.put("Accept", "application/json");
        headers.put("liszt-token", orderResponseData.getToken());

        CheckoutRequestData checkoutRequestData = GreenPayProcess.pack(cardData, orderResponseData);

        GsonRequest<CheckoutResponseData> postCheckoutRequest = new GsonRequest<CheckoutResponseData>(Request.Method.POST, checkoutURL , checkoutRequestData, CheckoutResponseData.class, headers, new Response.Listener<CheckoutResponseData>() {
            @Override
            public void onResponse(CheckoutResponseData response) {
                Gson gson = new GsonBuilder().disableHtmlEscaping().create();
                Log.d("GreenPay", "Checkout Order Response: " + gson.toJson(response));

                responseCallback.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("GreenPay", "GreenPay error");

                String body;
                String statusCode = String.valueOf(error.networkResponse.statusCode);
                Log.e("GreenPay", "GreenPay Error HTTP Error: " + statusCode);
                if(error.networkResponse.data!=null) {
                    try {
                        body = new String(error.networkResponse.data,"UTF-8");
                        Log.e("GreenPay", "GreenPay Error Body: " + body);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(postCheckoutRequest);
    }

    /**
     Nombre: pack
     Descripción: Realiza los cálculos y procesamiento de datos para generar el objeto CheckoutRequestData, requerido para hacer el proceso de Checkout en Greenpay.
     Retorna: El objeto CheckoutRequestData que contiene todos los datos requeridos para realizar el proces de Checkout en Greenpay.
     Parámetros: - cardData: Objeto con los datos de la tarjeta con la que se desea pagar.
                 - orderResponseData: Objeto que contiene la respuesta de la creación de la orden en Greenpay.
     */
    public static CheckoutRequestData pack(CreditCard cardData, BaseOrderResponseData orderResponseData){
        // 1. Generar la llave para el cifrado AES en modo CTR
        AESKey aesKey = CipherUtils.generateAESKey();
        byte[] keyBytes = aesKey.getKeyValue();

        // 2. Generar el contador (Counter) para el cifrado AES en modo CTR
        AESCounter aesCounter = CipherUtils.generateAESCounter();
        byte[] counterBytes = aesCounter.getCounterValue();

        // 3. Convertir los datos del objeto que contiene la tarjeta a string y luego a bytes
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String cardDataStr = gson.toJson(cardData);
        Log.d("GreenPay", "Checkout Data: " + cardDataStr);
        byte[] cardDataBytes = cardDataStr.getBytes();

        // 5    ### Cifrado con AES ###
        String aesHexValue = "";

        try {
            byte[] aesValue = AESUtils.cipherWithAESCTR(cardDataBytes, keyBytes, counterBytes);
            aesHexValue = StringUtils.byteArrayToHexString(aesValue);
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }

        // 6.   Obtener el valor LK (Liszt Key) requerido para la trama de Checkout.
        // 6.1  Se deben guardar los valores de la llave (como un arreglo de bytes) y el counter (como un valor nunérico entero).
        // 6.2  Al serializado los dos valores se obtiene algo como:
        //                      "{\"k\":[98,191,20,0,70,53,133,100,8,131,110,91,79,186,218,166],\"s\":138}"
        //      En donde el valor "k" contiene la llave AES y el valor "s" contiene el counter usados para cifrar AES en modo CTR del paso 5.
        // 6.3  Utilizar el cifrado asimétrico RSA con la llave pública brindada por Greenpay para cifrar la trama de 6.2
        //      El resultado se encuentra representado en base64.
        AESPair aesPair = new AESPair();
        aesPair.setKey(aesKey.getKey());
        aesPair.setCouner(aesCounter.getCounter());
        String aesPairStr = gson.toJson(aesPair);
        Log.d("GreenPay", "AES Pair Str: " + aesPairStr);

        String lk = "";

        try {
            lk = CipherUtils.cipherWithPublickKey(aesPairStr, GreenPayProcess.GreenPayPublicKey);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }

        // 8. Crea la entidad CheckoutRequestData con los datos de 6 y 7.
        CheckoutRequestData checkoutRequestData = new CheckoutRequestData();
        checkoutRequestData.setSession(orderResponseData.getSession());
        checkoutRequestData.setLd(aesHexValue);
        checkoutRequestData.setLk(lk);

        return checkoutRequestData;

    }

    /**
     Nombre: verifyCheckoutResponse
     Descripción: Valida que la respuesta obtenida por el proceso de Checkout de Greenpay fue generada en los servidores de Greenpay y no ha sido manipulada por terceras partes.
     Retorna: True si la firma de la respuesta del proceso de Checkout en Greenpay es correcta. Falso en caso contrario.
     Parámetros: - checkoutResponseData: Objeto con la respuesta del proceso de Checkout en Greenpay que contiene la firma a validar.
     */
    public static boolean verifyCheckoutResponse(CheckoutResponseData checkoutResponseData){
        // La firma contiene el status de la transacción y el número de orden, en el siguiente formato:
        String verifyValue = "status:" + checkoutResponseData.getStatus() + ",orderId:" + checkoutResponseData.getOrderId();

        boolean verified = CipherUtils.verifySignature(verifyValue, checkoutResponseData.getSignature(), GreenPayProcess.GreenPayPublicKey);
        if (verified){
            Log.d("GreenPay", "Signature verified");
        }else{
            Log.d("GreenPay", "Signature NOT verified");
        }

        return verified;
    }

    /**
     Nombre: postCreateOrder
     Descripción: Realiza el proceso de creación de una nueva orden en la plataforma Greenpay.
     Retorna: El objeto OrderResponseData que contiene la respuesta a la creación de la orden.
     Parámetros: - OrderResponseData: Objeto OrderRequestData con los datos para crear una nueva orden.
     - context - Contexto de Android para realizar los llamados al API DE GreenPay
     */
    public static void postTokenizeCardCreateOrder(TokenizeCardOrderRequestData orderRequestData, Context context, final ResponseCallback<TokenizeCardOrderResponseData> responseCallback){
        Log.d("GreenPay", "Creating Order:");
        String orderURL = "https://sandbox-merchant.GreenPay.me/tokenize";

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-type", "application/json");

        GsonRequest<TokenizeCardOrderResponseData> postTokenizeCardOrderRequest = new GsonRequest<TokenizeCardOrderResponseData>(Request.Method.POST, orderURL, orderRequestData, TokenizeCardOrderResponseData.class, headers, new Response.Listener<TokenizeCardOrderResponseData>() {
            @Override
            public void onResponse(TokenizeCardOrderResponseData response) {
                Gson gson = new GsonBuilder().disableHtmlEscaping().create();
                Log.d("GreenPay", "Tokenize Card Create Order Response: " + gson.toJson(response));

                responseCallback.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("GreenPay", "GreenPay error");

                String body;
                String statusCode = String.valueOf(error.networkResponse.statusCode);
                Log.e("GreenPay", "GreenPay Error HTTP Error: " + statusCode);
                if(error.networkResponse.data!=null) {
                    try {
                        body = new String(error.networkResponse.data,"UTF-8");
                        Log.e("GreenPay", "GreenPay Error Body: " + body);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(postTokenizeCardOrderRequest);
    }

    /**
     Nombre: PostTokenizeCardCheckout
     Descripción: Realiza el proceso de checkout en la plataforma Greenpay.
     Retorna: El objeto CheckoutResponseData que contiene la respuesta del proceso de Checkout.
     Parámetros: - cardData: Objeto que representa la tarjeta del cliente.
     - orderResponseData: Objeto orderResponseData con los datos para hacer checkout.
     - context - Contexto de Android para realizar los llamados al API DE GreenPay
     */
    public static void postTokenizeCardCheckout(CardData cardData, TokenizeCardOrderResponseData tokenizeCardOrderResponseData, Context context, final ResponseCallback<TokenizeCardCheckoutResponseData> responseCallback){
        Log.d("GreenPay", "Checking out Order:");
        String checkoutURL = "https://sandbox-checkout.greenpay.me/tokenize";

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-type", "application/json");
        headers.put("Accept", "application/json");
        headers.put("liszt-token", tokenizeCardOrderResponseData.getToken());

        CheckoutRequestData checkoutRequestData = GreenPayProcess.pack(cardData, tokenizeCardOrderResponseData);

        GsonRequest<TokenizeCardCheckoutResponseData> postCheckoutRequest = new GsonRequest<TokenizeCardCheckoutResponseData>(Request.Method.POST, checkoutURL , checkoutRequestData, TokenizeCardCheckoutResponseData.class, headers, new Response.Listener<TokenizeCardCheckoutResponseData>() {
            @Override
            public void onResponse(TokenizeCardCheckoutResponseData response) {
                Gson gson = new GsonBuilder().disableHtmlEscaping().create();
                Log.d("GreenPay", "Tokenize Card Checkout Order Response: " + gson.toJson(response));

                responseCallback.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("GreenPay", "GreenPay error");

                String body;
                String statusCode = String.valueOf(error.networkResponse.statusCode);
                Log.e("GreenPay", "GreenPay Error HTTP Error: " + statusCode);
                if(error.networkResponse.data!=null) {
                    try {
                        body = new String(error.networkResponse.data,"UTF-8");
                        Log.e("GreenPay", "GreenPay Error Body: " + body);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(postCheckoutRequest);
    }

    /**
     Nombre: verifyTokenizeCardCheckoutResponse
     Descripción: Valida que la respuesta obtenida por el proceso de Checkout de Greenpay fue generada en los servidores de Greenpay y no ha sido manipulada por terceras partes.
     Retorna: True si la firma de la respuesta del proceso de Checkout en Greenpay es correcta. Falso en caso contrario.
     Parámetros: - checkoutResponseData: Objeto con la respuesta del proceso de Checkout en Greenpay que contiene la firma a validar.
     */
    public static boolean verifyTokenizeCardCheckoutResponse(TokenizeCardCheckoutResponseData tokenizeCardCheckoutResponseData, String requestId){
        // La firma contiene el status de la transacción y el número de orden, en el siguiente formato:
        String verifyValue = "status:" + tokenizeCardCheckoutResponseData.getStatus() + ",requestId:" + requestId;

        boolean verified = CipherUtils.verifySignature(verifyValue, tokenizeCardCheckoutResponseData.getSignature(), GreenPayProcess.GreenPayPublicKey);
        if (verified){
            Log.d("GreenPay", "Tokenize Card Signature verified");
        }else{
            Log.d("GreenPay", "Tokenize Card Signature NOT verified");
        }

        return verified;
    }
}
