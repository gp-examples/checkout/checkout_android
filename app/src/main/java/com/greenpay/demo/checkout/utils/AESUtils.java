package com.greenpay.demo.checkout.utils;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESUtils {

    /**
     Nombre: cipherWithAESCTR
     Descripción: Realiza un cifrado simétrico AES CTR.
     Retorna: Un arreglo de bytes que representa el contenido cifrado.
     Parámetros: - plainTextAsByteArray: Contenido a cifrar representado como un arreglo de bytes.
                 - key: Llave para cifrado AES representada como un arreglo de bytes.
                 - counter: Contador para el cifrado AES CTR como un arreglo de bytes.
     */
    public static byte[] cipherWithAESCTR(byte[] plainTextAsByteArray, byte[] key, byte[] counter) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");

        SecretKey originalKey = new SecretKeySpec(key, 0, key.length, "AES");

        cipher.init(Cipher.ENCRYPT_MODE, originalKey, new IvParameterSpec(counter));
        byte[] cipheredValue = cipher.doFinal(plainTextAsByteArray);

        return cipheredValue;
    }
}
