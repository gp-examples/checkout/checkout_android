package com.greenpay.demo.checkout.utils;

import android.util.Base64;

import com.greenpay.demo.checkout.models.AESCounter;
import com.greenpay.demo.checkout.models.AESKey;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class CipherUtils {

    /**
     Nombre: generateAESKey
     Descripción: Genera un arreglo de enteros (0-255) aleatorios que funciona como llave privada para cifrado simétrico AES
     Retorna: Un arreglo de números enteros aleatorios que se usan como llave de cifrado.
     */
    public static AESKey generateAESKey(){
        int[] key = new int[16];
        Random random = new Random();
        for(int i=0; i< 16; ++i){
            int randomNumber = random.nextInt(256);
            key[i] = randomNumber;
        }

        AESKey aesKey = new AESKey(key);
        return aesKey;
    }

    /**
     Nombre: generateAESCounter
     Descripción: Genera un arreglo de enteros (0-255) aleatorios que funciona como llave privada para cifrado simétrico AES
     Retorna: Un arreglo de números enteros aleatorios que se usan como llave de cifrado.
     */
    public static AESCounter generateAESCounter(){
        // Un valor entre 0 y 255.
        AESCounter aesCounter = new AESCounter(new Random().nextInt(256));
        return aesCounter;
    }

    /**
     Nombre: cipherWithPublickKey
     Descripción: Cifra en RSA.
     Retorna: Una cadena en base64 con los datos cifrados con la llave pública.
     Parámetros: - textToCipher: Texto a cifrar.
                 - publicKeyContent: Llave pública en formato String.
     */
    public static String cipherWithPublickKey(String textToCipher, String publicKeyContent) throws NoSuchAlgorithmException,
            NoSuchPaddingException,
            InvalidKeyException,
            IllegalBlockSizeException,
            BadPaddingException,
            InvalidKeySpecException {

        PublicKey rsaPublicKey = CipherUtils.stringToPublicKey(publicKeyContent);

        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, rsaPublicKey);
        byte[] encryptedBytes = cipher.doFinal(textToCipher.getBytes());

        return Base64.encodeToString(encryptedBytes, Base64.NO_WRAP);
    }

    /**
     Nombre: stringToPublicKey
     Descripción: Convierte una llave pública en String a un objeto PublicKey para cifrado RSA.
     Retorna: Un objeto PublicKey que representa la llave pública de cifrado RSA.
     Parámetros: - publicKeyString: Llave pública como String.
     */
    public static PublicKey stringToPublicKey(String publicKeyString)
            throws NoSuchAlgorithmException,
            NoSuchPaddingException,
            InvalidKeyException,
            IllegalBlockSizeException,
            BadPaddingException,
            InvalidKeySpecException{

        try {
            if (publicKeyString.contains("-----BEGIN PUBLIC KEY-----") || publicKeyString.contains("-----END PUBLIC KEY-----"))
                publicKeyString = publicKeyString.replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");
            byte[] keyBytes = Base64.decode(publicKeyString, Base64.DEFAULT);
            X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");

            return keyFactory.generatePublic(spec);

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();

            return null;
        }
    }

    /**
     Nombre: verifySignature
     Descripción: Valida la firma de una transacción usando SHA256withRSA.
     Retorna: True si la firma es correcta. False en caso contrario.
     Parámetros: - valueToVerify: Valor String a verificar.
                 - referenceValue: Valor en String base64 contra el cual comparar la firma
                 - publicKeyStr: Llave pública como un String.
     */
    public static boolean verifySignature(String valueToVerify, String referenceValue, String publicKeyStr){
        boolean result = false;
        try {
            byte[] msgBytes = valueToVerify.getBytes();
            byte[] sigBytes = StringUtils.convertHexStringToByteArray(referenceValue);

            PublicKey publicKey = CipherUtils.stringToPublicKey(publicKeyStr);
            Signature sha256WithRSA = Signature.getInstance("SHA256withRSA");
            sha256WithRSA.initVerify(publicKey);
            sha256WithRSA.update(msgBytes);
            result = sha256WithRSA.verify(sigBytes);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
