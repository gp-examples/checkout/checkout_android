package com.greenpay.demo.checkout.utils;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.*;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Map;

public class GsonRequest<T> extends Request<T> {
    private final Gson gson = new GsonBuilder().disableHtmlEscaping().create();
    private final Class<T> clazz;
    private final Map<String, String> headers;
    private final Listener<T> listener;

    private final Object requestBodyObj;

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param url URL of the request to make
     * @param clazz Relevant class object, for Gson's reflection
     * @param headers Map of request headers
     */
    public GsonRequest(int method, String url, Object requestBodyObj, Class<T> clazz, Map<String, String> headers,
                       Listener<T> listener, ErrorListener errorListener) {
        super(method, url, errorListener);
        this.clazz = clazz;
        this.headers = headers;
        this.listener = listener;
        this.requestBodyObj = requestBodyObj;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Log.d("GreenPay", "Getting Headers");
        Map<String, String> transHeaders = headers != null ? headers : super.getHeaders();
        for (String key : transHeaders.keySet() ) {
            Log.d("GreenPay", key + " : " + transHeaders.get(key));
        }
        return transHeaders;
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            Log.d("GreenPay", "JSON Response: " + json);
            return Response.success(
                    gson.fromJson(json, clazz),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }

    public byte[] getBody() throws AuthFailureError {
        String requestAsStr = gson.toJson(requestBodyObj);

        Log.d("GreenPay", "Converting Body class to Str: " + requestAsStr);

        return requestAsStr.getBytes();
    }
}