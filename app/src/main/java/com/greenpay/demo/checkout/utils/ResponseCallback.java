package com.greenpay.demo.checkout.utils;

public interface ResponseCallback<T> {
    void onSuccess(T response);
}
