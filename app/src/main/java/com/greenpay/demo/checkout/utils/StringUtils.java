package com.greenpay.demo.checkout.utils;

import java.nio.charset.StandardCharsets;

public class StringUtils {

    /**
     Nombre: hexStringtoString
     Descripción: Convierte una cadena en formato Hexadecimal en una cadena regular.
     Retorna: Una cada regular que representa la cadena en formato Hexadecimal de entrada.
     */
    public static String hexStringtoString(String hexString){
        int l = hexString.length();
        byte[] data = new byte[l/2];
        for (int i = 0; i < l; i += 2) {
            data[i/2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4)
                    + Character.digit(hexString.charAt(i+1), 16));
        }
        return new String(data, StandardCharsets.UTF_8);
    }

    /**
     Nombre: byteArrayToHexString
     Descripción: Convierte un arreglo de bytes en una cadena de caracteres en formato Hexadecimal.
     Retorna: La representación Hexadecimal del arreglo de bytes parámetro.
     */
    public static String byteArrayToHexString(byte[] array){
        StringBuffer buffer = new StringBuffer();
        for(int i=0; i < array.length; i++){
            buffer.append(Character.forDigit((array[i] >> 4) & 0xF, 16));
            buffer.append(Character.forDigit((array[i] & 0xF), 16));
        }
        return buffer.toString();
    }

    /**
     Nombre: convertHexStringToByteArray
     Descripción: Convierte una cadena de caracteres en formato Hexadecimal en su representación como arreglo de bytes.
     Retorna: La representación como arreglo de bytes de la cadena Hexadecimal parámetro.
     */
    public static byte[] convertHexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len/2];

        for(int i = 0; i < len; i+=2){
            data[i/2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i+1), 16));
        }

        return data;
    }
}
